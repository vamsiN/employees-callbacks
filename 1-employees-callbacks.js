/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
*/

// const data = fs.readFileSync('data.json','utf8')
// const dataObject = JSON.parse(data)
// console.log(dataObject)

const fs = require('fs')


const readFilePromise = (file) => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf-8', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    })
}

const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, (error => {
            if (error) {
                reject(error)
            } else {
                resolve(`${file} file created`)
            }
        }))
    })
}

// ---------- 1st problem --------------

readFilePromise('./data.json').then((response) => {
    return JSON.parse(response)
}).then((jsonData) => {

    const dataOfSpecificIds = jsonData["employees"].filter((each) => {
        return each.id == 2 || each.id == 13 || each.id == 23
    })
    return dataOfSpecificIds

}).then((dataOfSpecificIds) => {
    return JSON.stringify(dataOfSpecificIds)

}).then((dataOfSpecificIdsJson) => {
    return writeFilePromise('./dataOfSpecificIds.json', dataOfSpecificIdsJson)

}).then(()=>{
    //console.log(`dataOfSpecificIds.json file created`)
}).catch((error) => {
    console.log(error)
})

// ---------- 2nd problem --------------

readFilePromise('./data.json').then((response) => {
    return JSON.parse(response)
}).then((jsonData)=>{
    // console.log(jsonData['employees'])
    const employeesArray = jsonData['employees']

    const groupedEmployees = employeesArray.reduce((acc,cur)=>{
        if(acc[cur.company] === undefined){
            acc[cur.company] = [];
        } else {
            acc[cur.company].push(cur);
        }
        return acc;
    },{})
    //console.log(groupedEmployees)
    
    return writeFilePromise('./groupedEmployees.json', JSON.stringify(groupedEmployees))

}).then(()=>{
    //console.log(`groupedEmployees.json file created`)
}).catch((error) => {
    console.log(error)
})

// ---------- 3rd problem --------------

readFilePromise('./data.json').then((response) => {
    return JSON.parse(response)
}).then((jsonData)=>{
    const employeesArray = jsonData['employees']
    const PowerpuffEmployees = employeesArray.filter((each)=>{
        return each.company === 'Powerpuff Brigade'
    })

    //console.log(PowerpuffEmployees)
    return writeFilePromise('./PowerpuffEmployees.json', JSON.stringify(PowerpuffEmployees))

}).then(()=>{
    //console.log(`PowerpuffEmployees.json file created`)
}).catch((error) => {
    console.log(error)
})

//------------- 4th problem ------------

readFilePromise('./data.json').then((response) => {
    return JSON.parse(response)
}).then((jsonData)=>{
    const employeesArray = jsonData['employees']
    employeesArray.filter((each)=>{
        return each.id != 2
    })

    console.log(employeesArray)
    return writeFilePromise('./modifiedEmployeesArray.json', JSON.stringify(employeesArray))

}).then(()=>{
    console.log(`modifiedEmployeesArray.json file created`)
}).catch((error) => {
    console.log(error)
})